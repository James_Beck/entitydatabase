﻿using Microsoft.AspNetCore.Mvc;

namespace TheEntity.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
