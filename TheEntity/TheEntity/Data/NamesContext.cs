﻿using TheEntity.Models;
using Microsoft.EntityFrameworkCore;

namespace TheEntity.Data
{
    public class NamesContext : DbContext
    {
        public NamesContext(DbContextOptions<NamesContext> options) : base(options)
        { }

        public DbSet<Names> Names { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Names>().ToTable("Names");
        }
    }
}
