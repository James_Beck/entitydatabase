﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEntity.Models;

namespace TheEntity.Data
{
    public class DbInitializer
    {
        public static void Initialize(NamesContext context)
        {
            context.Database.EnsureCreated();

            if (context.Names.Any())
            {
                return;
            }

            var names = new Names[]
            {
                new Names {FirstName = "James", LastName = "Beck" },
                new Names {FirstName = "Guri", LastName = "Ghuman" },
                new Names {FirstName = "Cory", LastName = "Bruin" },
                new Names {FirstName = "Matt", LastName = "Collecutt" }
            };

            foreach (Names n in names)
            {
                context.Names.Add(n);
            }

            context.SaveChanges();
        }
    }
}
